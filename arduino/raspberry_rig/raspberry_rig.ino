#include "pyCommsLib.h"
#include "HX711.h"

String msgName[] = {"loadcell", "pressure"};
String dataCarrier[2];

// Loadcell
HX711 loadcell;

uint8_t dataPin = 3;
uint8_t clockPin = 2;

volatile float load;

// Pressure
float zero = 0;
 

void setup() {
  Serial.begin(115200);

  loadcell.begin(dataPin, clockPin);
  loadcell.set_scale(760);
  loadcell.tare(); 

  init_python_communication();

  for(int i = 0; i<500; i++) { 
    zero += analogRead(0);
  }
  zero /= 500;
}


void loop() {
  float pressure = 0;
  for(int i = 0; i<100; i++){
    pressure += analogRead(0);
  }
  pressure /= 100;
  pressure -= zero;

  dataCarrier[0] = String(loadcell.get_units());
  dataCarrier[1] = String(pressure);

  load_msg_to_python(msgName, dataCarrier, size_of_array(msgName));
  sync(); 
}
