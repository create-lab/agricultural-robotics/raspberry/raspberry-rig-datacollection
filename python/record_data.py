from comms_wrapper import Arduino, Key
from utility import *
from time import time

def main():
    save_dir = define_csv_save_location("../data/", "name_of_folder")

    key = Key()

    while 1:
        filename = obtain_csv_filename(save_dir)

        rasp_arduino = Arduino("Raspberry rig Arduino", "/dev/ttyACM0", 115200)
        rasp_arduino.connect_and_handshake()

        data_capture = {"time":[], "pressure":[], "loadcell":[]} 
        

        print("\n\n---------------------")
        print("  Recording  ")
        print("---------------------\n")
        print("--> Press 'q' to end recording")

        timer = time() 
        while 1:
            rasp_arduino.receive_message()

            if rasp_arduino.newMsgReceived:
                t = time() - timer
                msg = rasp_arduino.receivedMessages
                
                # Store data
                data_capture["time"].append(t)
                data_capture["pressure"].append(float(msg["pressure"]))
                data_capture["loadcell"].append(float(msg["loadcell"]))

            if key.keyPress == "q":
                break

        # Plot and save data
        plot_and_save_data(plottingData= ([data_capture["time"], data_capture["pressure"]], 
                                [data_capture["time"], data_capture["loadcell"]]),
                                xAxisLabel= ("Time (s)", "Time (s)"), 
                                yAxisLabel =("Pressure (arb unit)", "Loadcell (g)"),
                                label = (["Pressure"], ["Loadcell"]), 
                                savingData= (data_capture["time"], data_capture["pressure"], data_capture["loadcell"]), 
                                savingData_names = ["time", "pressure", "loadcell"],
                                filename= filename,
                                saveDir= save_dir,
                                display_plot= True, 
                                saveData = True, 
                                figsize = (6,8))
        

if __name__ == "__main__":
    main()